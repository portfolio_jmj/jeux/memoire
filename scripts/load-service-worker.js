if ("serviceWorker" in navigator) {
    navigator.serviceWorker.register("/pwa/service-worker.js").then(serviceWorker => {
      console.log("Service Worker registered: ", serviceWorker);
    });
  }
  