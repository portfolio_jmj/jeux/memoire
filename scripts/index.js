//container ou on rajoutera dynamiquement les cartes
/** @type {HTMLElement} */
const container = document.getElementById("container");
const labelChooseItem = document.getElementById("labelChooseItem");
const InputChooseItem = document.getElementById("chooseItem");
const SpanLevel = document.getElementById("level");
const DialogWinAlert = document.getElementById("winAlert");
const SubmitBtn = document.getElementById("submitBtn");
const cardWidth = 110;
let itemArray = [];
let nbItem;
let lastValue = null;
let LastId = null;
let countClick = 0;
letcountCardFind = 0;

initFormLevel();

/**
* Démarrage du programm
*/
function main() {
    countCardFind = 0;
    nbItem = InputChooseItem.value;
    chooseItem(nbItem);

    itemArray.forEach((element, index) => {
        buildItemAndAttach(element, index)
    });
    container.style.display = "flex";
    SubmitBtn.innerText = "Recommencer"
}


/**
 * renvoie une liste random d'item en prenant
 * en compte la difficulté
 * @param {number} difficulté
 */
function chooseItem(difficulté) {
    if (difficulté % 4 != 0 || difficulté < 3)
        throw new Error("La difficulté doit être un multiple de 2 et minimun 8");

    const numberItem = difficulté / 2;
    const itemRandom = [];

    let loop = true;
    let index = 0;
    while (loop) {
        const rand = Math.floor(Math.random() * 100);
        if (itemRandom.indexOf(rand) != -1) continue;

        itemRandom[index] = Math.floor(Math.random() * 100);
        index++;

        if ((index == numberItem))
            loop = false;
    }
    container.innerHTML = '';
    itemArray = [...itemRandom, ...itemRandom] //doubles les cartes
    fisherYatesShuffle(itemArray); //les randomises

}

/**
 * quand input rangge change de valeur
 * @param {number} e 
 */
function handleOnChangeRange(e) {
    let vLevel = "💖"
    switch (true) {
        case e == 0:
            initFormLevel();
            return;
        case e > 24 && e <= 40:
            vLevel = "💖💖"
            break;

        case e > 40:
            vLevel = "💖💖💖"
            break;
        default:
            break;
    }
    labelChooseItem.innerText = `Nombre de tuille : ${e}`;
    SpanLevel.innerText = `Difficulté : ${vLevel}`;
    SubmitBtn.style.opacity = 1;
    SpanLevel.style.display = "grid";

}


/**
 * Build element
 * @param {number} value 
 */
function buildItemAndAttach(value, index) {
    const div = document.createElement("div");
    div.setAttribute("id", `${value}-${index}`);
    div.innerText = value;
    div.classList.add("card");
    div.addEventListener('click', handleOnClick)
    container.appendChild(div);
}

function handleOnClick(e) {
    if (!limitDoubleSelected())
        return;

    //récupération de l'ancienne carte
    const lastCard = document.getElementById(LastId);

    if(lastCard == e.target)
        return;
    
    value = e.target.innerText;

    var msg = new SpeechSynthesisUtterance(value);
    speechSynthesis.speak(msg); 

    e.target.classList.toggle('cardSelected');
    if (value == lastValue) {
        e.target.classList.add('cardFind');
        e.target.classList.remove('cardSelected');
        e.target.setAttribute('click', '');
        lastCard.classList.add('cardFind');
        lastCard.classList.remove('cardSelected');
        lastCard.setAttribute('click', '');
        lastValue = null;
        LastId = null;
        e.target.removeEventListener('click', handleOnClick);
        lastCard.removeEventListener('click', handleOnClick);
        checkVictory();
    }
    else {
        if (lastCard == null) {
            LastId = e.target.id;
            lastValue = value;
            return;
        }

        lastValue = null;
        LastId = null;

        setTimeout(() => {
            e.target.classList.toggle('cardSelected');
            lastCard.classList.toggle('cardSelected');
        }, 700);
    }

}

/**
 * Vérifie si on n'as pas déjà deux cartes retournées
 * @returns 
 */
function limitDoubleSelected() {
    const count = document.querySelectorAll('.cardSelected').length;
    if (count == 2)
        return false;

    return true;

}

/**
 * Vérifie si on est en état de victoire
 */
function checkVictory() {
    setTimeout(() => {
        countCardFind = countCardFind + 2;
        if (nbItem == countCardFind) {
            DialogWinAlert.showModal();
            document.body.classList.add('blur');
        }
    }, 550);

}

/**
 * Permet de randomiser l'ordre des élements d'un tableau
 * @param {[]} arr 
 */
function fisherYatesShuffle(arr) {
    for (var i = arr.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1)); //random index
        [arr[i], arr[j]] = [arr[j], arr[i]]; // swap
    }
}

function initFormLevel() {
    labelChooseItem.innerText = "Séléctionne le nombre de tuilles"
    labelChooseItem.style.gridColumnEnd = "span2";
    SpanLevel.style.display = "none";
    SubmitBtn.style.opacity = 0;
}